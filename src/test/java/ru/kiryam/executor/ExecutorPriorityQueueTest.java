package ru.kiryam.executor;

import org.joda.time.LocalDateTime;
import ru.kiryam.executor.impl.EventBase;
import ru.kiryam.executor.impl.ExecutorPriorityQueue;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

import static junit.framework.TestCase.assertTrue;


public class ExecutorPriorityQueueTest {

    /*
        event2 date < event1 date
        event3 date = event4 date

        Events should be run in following order:
        event2 ( 2016-01-01 00:00:00 )
        event1 ( 2016-01-02 00:00:00 )
        event3 ( 2016-01-03 00:00:00 )
        event4 ( 2016-01-03 00:00:00 )
        event5 ( 2016-01-03 00:00:00 )
     */
    @org.junit.Test
    public void testPush() throws Exception {
        ExecutorPriorityQueue executorPriorityQueue = new ExecutorPriorityQueue();

        final AtomicBoolean event1Called = new AtomicBoolean(false);
        final AtomicBoolean event2Called = new AtomicBoolean(false);

        Event event1 = new EventBase(new LocalDateTime(2016,1,1,2,0,0,0), new Callable() {
            public Object call() throws Exception {
                assertTrue(event2Called.get());
                event1Called.set(true);
                return null;
            }
        });
        executorPriorityQueue.push(event1);


        Event event2 = new EventBase(new LocalDateTime(2016,1,1,1,0,0,0), new Callable() {
            public Object call() throws Exception {
                event2Called.set(true);
                return null;
            }
        });
        executorPriorityQueue.push(event2);

        final AtomicBoolean event3Called = new AtomicBoolean(false);
        Event event3 = new EventBase(new LocalDateTime(2016,1,1,3,0,0,0), new Callable() {
            public Object call() throws Exception {
                event3Called.set(true);
                return null;
            }
        });
        executorPriorityQueue.push(event3);


        final AtomicBoolean event4Called = new AtomicBoolean(false);
        Event event4 = new EventBase(new LocalDateTime(2016,1,1,3,0,0,0), new Callable() {
            public Object call() throws Exception {
                assertTrue(event3Called.get());
                event4Called.set(true);
                return null;
            }
        });
        executorPriorityQueue.push(event4);

        final AtomicBoolean event5Called = new AtomicBoolean(false);
        Event event5 = new EventBase(new LocalDateTime(2016,1,1,3,0,0,0), new Callable() {
            public Object call() throws Exception {
                assertTrue(event4Called.get());
                event5Called.set(true);
                return null;
            }
        });
        executorPriorityQueue.push(event5);

        long started = System.currentTimeMillis();
        executorPriorityQueue.start();


        while (!event1Called.get() || !event2Called.get() || !event3Called.get() || !event4Called.get() ){
            if (System.currentTimeMillis()-started > 3000 ){ // timeout 3s
                throw new Exception("Timeout");
            }
            Thread.sleep(10);
        }
    }
}