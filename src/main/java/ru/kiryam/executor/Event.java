package ru.kiryam.executor;


import org.joda.time.LocalDateTime;

import java.util.concurrent.Callable;

public interface Event extends Comparable<Object> {
    LocalDateTime getDateTime();
    Callable getCallable();
}
