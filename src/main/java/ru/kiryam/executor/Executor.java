package ru.kiryam.executor;


public interface Executor {
    void start();
    void stop();
    void push(Event event);
}
