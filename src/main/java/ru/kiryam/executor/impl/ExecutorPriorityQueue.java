package ru.kiryam.executor.impl;

import ru.kiryam.executor.Caller;
import ru.kiryam.executor.Event;
import ru.kiryam.executor.Executor;

import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;


public class ExecutorPriorityQueue implements Executor, Runnable {
    private Queue<Event> queue = new PriorityBlockingQueue<Event>();
    private final ArrayList<Caller> callers = new ArrayList<Caller>();
    private boolean stop = false;

    public void start() {
        // TODO load state here
        
        Caller caller = new CallerBase(queue);
        callers.add(caller);
        Thread thread = new Thread(caller);
        thread.start();
    }


    public void stop() {
        stop = true;
        for(Caller caller : callers){
            caller.stop();
        }

        // TODO save state here;
    }


    public void push(Event event) {
        queue.add(event);
    }

    public void run() {
        while ( !stop ){
            // restart child if need
            // stats collector

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
