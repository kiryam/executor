package ru.kiryam.executor.impl;


import org.joda.time.LocalDateTime;
import ru.kiryam.executor.Caller;
import ru.kiryam.executor.Event;

import java.util.Queue;

public class CallerBase implements Caller {
    private final Queue queue;
    private boolean stop = false;

    public CallerBase(Queue queue) {
        this.queue = queue;
    }

    public void run() {
        while( !stop ){

            Event testEvent = (Event)queue.peek();
            LocalDateTime now = LocalDateTime.now();
            if ( testEvent != null && (testEvent.getDateTime().isBefore(now) || testEvent.getDateTime().equals(now)) ){
                if( queue.remove(testEvent) ) { // Если никто раньше не смог забрать этот эвент
                    try {
                        testEvent.getCallable().call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }else{
                // wait for new events
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void stop() {
        stop = true;
    }
}
