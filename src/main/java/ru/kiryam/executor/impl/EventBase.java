package ru.kiryam.executor.impl;


import org.joda.time.LocalDateTime;
import org.joda.time.ReadablePartial;
import ru.kiryam.executor.Event;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;

public class EventBase implements Event{
    private final LocalDateTime localDateTime;
    private final Callable callable;
    final static AtomicLong seq = new AtomicLong();
    public final long seqNum;

    public EventBase(LocalDateTime localDateTime, Callable callable) {
        this.seqNum = seq.getAndIncrement();
        this.localDateTime = localDateTime;
        this.callable = callable;
    }

    public LocalDateTime getDateTime() {
        return localDateTime;
    }

    public Callable getCallable() {
        return callable;
    }

    public int compareTo(Object partial) {
        EventBase eventBase = (EventBase) partial;
        int res = localDateTime.compareTo(eventBase.getDateTime());

        if (res == 0 && partial != this)
            res = (seqNum < eventBase.seqNum ? -1 : 1);
        return res;
    }
}
